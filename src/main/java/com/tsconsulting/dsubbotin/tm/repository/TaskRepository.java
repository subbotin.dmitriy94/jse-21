package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public boolean existById(final String userId, final String id) throws AbstractException {
        return findById(userId, id) != null;
    }

    @Override
    public Task findByName(final String userId, final String name) throws AbstractException {
        return findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public void removeByName(final String userId, final String name) throws AbstractException {
        final Optional<Task> optional = Optional.ofNullable(findByName(userId, name));
        optional.ifPresent(this::remove);
    }

    @Override
    public void updateBuyId(
            final String userId,
            final String id,
            final String name,
            final String description
    ) throws AbstractException {
        final Optional<Task> optional = Optional.ofNullable(findById(userId, id));
        optional.ifPresent(t -> {
            t.setName(name);
            t.setDescription(description);
        });
    }

    @Override
    public void updateBuyIndex(
            final String userId,
            final int index,
            final String name,
            final String description
    ) throws AbstractException {
        final Task task = findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
    }

    @Override
    public void startById(final String userId, final String id) throws AbstractException {
        final Optional<Task> optional = Optional.ofNullable(findById(userId, id));
        optional.ifPresent(t -> {
            t.setStatus(Status.IN_PROGRESS);
            t.setStartDate(new Date());
        });
    }

    @Override
    public void startByIndex(final String userId, final int index) throws AbstractException {
        final Task task = findByIndex(userId, index);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
    }

    @Override
    public void startByName(final String userId, final String name) throws AbstractException {
        final Optional<Task> optional = Optional.ofNullable(findByName(userId, name));
        optional.ifPresent(t -> {
            t.setStatus(Status.IN_PROGRESS);
            t.setStartDate(new Date());
        });
    }

    @Override
    public void finishById(final String userId, final String id) throws AbstractException {
        final Optional<Task> optional = Optional.ofNullable(findById(userId, id));
        optional.ifPresent(t -> t.setStatus(Status.COMPLETED));
    }

    @Override
    public void finishByIndex(final String userId, final int index) throws AbstractException {
        final Task task = findByIndex(userId, index);
        task.setStatus(Status.COMPLETED);
    }

    @Override
    public void finishByName(final String userId, final String name) throws AbstractException {
        final Optional<Task> optional = Optional.ofNullable(findByName(userId, name));
        optional.ifPresent(t -> t.setStatus(Status.COMPLETED));
    }

    @Override
    public void updateStatusById(
            final String userId,
            final String id,
            final Status status
    ) throws AbstractException {
        final Optional<Task> optional = Optional.ofNullable(findById(userId, id));
        optional.ifPresent(t -> {
                    t.setStatus(status);
                    if (status == Status.IN_PROGRESS) t.setStartDate(new Date());
                });
    }

    @Override
    public void updateStatusByIndex(
            final String userId,
            final int index,
            final Status status
    ) throws AbstractException {
        final Task task = findByIndex(userId, index);
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
    }

    @Override
    public void updateStatusByName(
            final String userId,
            final String name,
            final Status status
    ) throws AbstractException {
        final Optional<Task> optional = Optional.ofNullable(findByName(userId, name));
        optional.ifPresent(t -> {
                    t.setStatus(status);
                    if (status == Status.IN_PROGRESS) t.setStartDate(new Date());
                });
    }

    @Override
    public void bindTaskToProjectById(
            final String userId,
            final String projectId,
            final String taskId
    ) throws AbstractException {
        final Optional<Task> optional = Optional.ofNullable(findById(userId, taskId));
        optional.ifPresent(t -> t.setProjectId(projectId));
    }

    @Override
    public void unbindTaskById(final String userId, final String id) throws AbstractException {
        final Optional<Task> optional = Optional.ofNullable(findById(userId, id));
        optional.ifPresent(t -> t.setProjectId(null));
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String id) throws AbstractException {
        return findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(id))
                .collect(Collectors.toList());

    }

    @Override
    public void removeAllTaskByProjectId(final String userId, final String id) {
        findAll(userId).stream()
                .filter(t -> id.equals(t.getProjectId()))
                .forEach(t -> t.setProjectId(null));
    }

}
