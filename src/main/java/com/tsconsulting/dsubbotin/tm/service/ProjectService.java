package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;

public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String userId, final String name) throws AbstractException {
        checkName(name);
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(
            final String userId,
            final String name,
            final String description
    ) throws AbstractException {
        checkName(name);
        if (EmptyUtil.isEmpty(description)) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(project);
    }

    @Override
    public Project findByName(final String userId, final String name) throws AbstractException {
        checkName(name);
        return projectRepository.findByName(userId, name);
    }

    @Override
    public void updateById(
            final String userId,
            final String id,
            final String name,
            final String description
    ) throws AbstractException {
        checkIndex(id);
        checkName(name);
        projectRepository.updateBuyId(userId, id, name, description);
    }

    @Override
    public void updateByIndex(
            final String userId,
            final int index,
            final String name,
            final String description
    ) throws AbstractException {
        if (index < 0) throw new EmptyIndexException();
        checkName(name);
        projectRepository.updateByIndex(userId, index, name, description);
    }

    @Override
    public void startById(final String userId, final String id) throws AbstractException {
        checkIndex(id);
        projectRepository.startById(userId, id);
    }

    @Override
    public void startByIndex(final String userId, final int index) throws AbstractException {
        checkIndex(index);
        projectRepository.startByIndex(userId, index);
    }

    @Override
    public void startByName(final String userId, final String name) throws AbstractException {
        checkName(name);
        projectRepository.startByName(userId, name);
    }

    @Override
    public void finishById(final String userId, final String id) throws AbstractException {
        checkIndex(id);
        projectRepository.finishById(userId, id);
    }

    @Override
    public void finishByIndex(final String userId, final int index) throws AbstractException {
        checkIndex(index);
        projectRepository.finishByIndex(userId, index);
    }

    @Override
    public void finishByName(final String userId, final String name) throws AbstractException {
        checkName(name);
        projectRepository.finishByName(userId, name);
    }

    @Override
    public void updateStatusById(
            final String userId,
            final String id,
            final Status status
    ) throws AbstractException {
        checkIndex(id);
        projectRepository.updateStatusById(userId, id, status);
    }

    @Override
    public void updateStatusByIndex(
            final String userId,
            final int index,
            final Status status
    ) throws AbstractException {
        checkIndex(index);
        projectRepository.updateStatusByIndex(userId, index, status);
    }

    @Override
    public void updateStatusByName(
            final String userId,
            final String name,
            final Status status
    ) throws AbstractException {
        checkName(name);
        projectRepository.updateStatusByName(userId, name, status);
    }

    private void checkIndex(final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

    private void checkName(final String name) throws EmptyNameException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
    }

}
