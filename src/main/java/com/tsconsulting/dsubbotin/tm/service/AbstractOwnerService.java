package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IOwnerRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IOwnerService;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractOwnerEntity;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> implements IOwnerService<E> {

    protected final IOwnerRepository<E> ownerRepository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        this.ownerRepository = repository;
    }

    @Override
    public void add(final E entity) throws AbstractException {
        if (entity == null) throw new ProjectNotFoundException();
        ownerRepository.add(entity);
    }

    @Override
    public void remove(final String userId, final E entity) throws AbstractException {
        if (entity == null) throw new ProjectNotFoundException();
        ownerRepository.remove(userId, entity);
    }

    @Override
    public void clear(final String userId) {
        ownerRepository.clear(userId);
    }

    @Override
    public List<E> findAll(final String userId) {
        return ownerRepository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return ownerRepository.findAll(userId, comparator);
    }

    @Override
    public E findById(final String userId, final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        return ownerRepository.findById(userId, id);
    }

    @Override
    public E findByIndex(final String userId, final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        return ownerRepository.findByIndex(userId, index);
    }

    @Override
    public void removeById(final String userId, final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        ownerRepository.removeById(userId, id);
    }

    @Override
    public void removeByIndex(final String userId, final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        ownerRepository.removeByIndex(userId, index);
    }

}
