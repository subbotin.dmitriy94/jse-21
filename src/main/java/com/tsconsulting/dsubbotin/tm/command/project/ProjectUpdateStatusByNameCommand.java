package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownStatusException;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectUpdateStatusByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-update-status-by-name";
    }

    @Override
    public String description() {
        return "Update status project by name.";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findByName(currentUserId, name);
        TerminalUtil.printMessage("Enter status:");
        TerminalUtil.printMessage(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        Status status = EnumerationUtil.parseStatus(statusValue);
        if (status == null) throw new UnknownStatusException();
        serviceLocator.getProjectService().updateStatusByName(currentUserId, name, status);
        TerminalUtil.printMessage("[Updated project status]");
    }

}
