package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Override
    public String description() {
        return "Finish project by id.";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().finishById(currentUserId, id);
        TerminalUtil.printMessage("[Project complete]");
    }

}
