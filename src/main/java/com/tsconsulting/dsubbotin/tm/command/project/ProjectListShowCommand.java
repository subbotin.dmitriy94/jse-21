package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Display project list.";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter sort:");
        TerminalUtil.printMessage(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        Sort sortType = EnumerationUtil.parseSort(sort);
        List<Project> projects;
        projects =
                sortType == null
                        ? serviceLocator.getProjectService().findAll(currentUserId)
                        : serviceLocator.getProjectService().findAll(currentUserId, sortType.getComparator());
        int index = 1;
        for (Project project : projects) TerminalUtil.printMessage(index++ + ". " + project);
    }

}
