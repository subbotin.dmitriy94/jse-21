package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class UserShowByIdCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-show-by-id";
    }

    @Override
    public String description() {
        return "Display user by id.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        showUser(user);
    }

}
