package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class UserUpdateByIdCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-update-by-id";
    }

    @Override
    public String description() {
        return "Update user by id.";
    }

    @Override
    public void execute() throws AbstractException {
        final String id = serviceLocator.getAuthService().getCurrentUserId();
        serviceLocator.getUserService().findById(id);
        TerminalUtil.printMessage("Enter last name:");
        final String lastName = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter first name:");
        final String firstName = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter middle name:");
        final String middleName = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter email:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateById(id, lastName, firstName, middleName, email);
        TerminalUtil.printMessage("User updated");
    }

}
