package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    public String description() {
        return "Finish task by id.";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().finishById(currentUserId, id);
        TerminalUtil.printMessage("[Updated task status]");
    }

}
