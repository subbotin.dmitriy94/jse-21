package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class ProjectFinishByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-finish-by-name";
    }

    @Override
    public String description() {
        return "Finish project by name.";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().finishByName(currentUserId, name);
        TerminalUtil.printMessage("[Project complete]");
    }

}
