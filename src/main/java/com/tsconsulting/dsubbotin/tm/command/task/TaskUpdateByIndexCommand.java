package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskService().findByIndex(currentUserId, index);
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter description:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateByIndex(currentUserId, index, name, description);
        TerminalUtil.printMessage("[Task updated]");
    }

}
