package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class UserLogOutCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-logout";
    }

    @Override
    public String description() {
        return "User log out.";
    }

    @Override
    public void execute() throws AbstractException {
        serviceLocator.getAuthService().logout();
        TerminalUtil.printMessage("Exit done.");
    }

}
