package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-start-by-index";
    }

    @Override
    public String description() {
        return "Start task by index.";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskService().startByIndex(currentUserId, index);
        TerminalUtil.printMessage("[Updated task status]");
    }

}
