package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class ProjectFinishByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-finish-by-index";
    }

    @Override
    public String description() {
        return "Finish project by index.";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getProjectService().finishByIndex(currentUserId, index);
        TerminalUtil.printMessage("[Project complete]");
    }

}
