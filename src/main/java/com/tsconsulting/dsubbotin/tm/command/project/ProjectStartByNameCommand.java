package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Override
    public String description() {
        return "Start project by name.";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().startByName(currentUserId, name);
        TerminalUtil.printMessage("[Project started]");
    }

}
