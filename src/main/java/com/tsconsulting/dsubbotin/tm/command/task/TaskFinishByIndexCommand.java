package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class TaskFinishByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-finish-by-index";
    }

    @Override
    public String description() {
        return "Finish task by index.";
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskService().finishByIndex(currentUserId, index);
        TerminalUtil.printMessage("[Updated task status]");
    }

}
