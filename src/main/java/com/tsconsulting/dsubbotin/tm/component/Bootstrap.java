package com.tsconsulting.dsubbotin.tm.component;

import com.tsconsulting.dsubbotin.tm.api.repository.*;
import com.tsconsulting.dsubbotin.tm.api.service.*;
import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.project.*;
import com.tsconsulting.dsubbotin.tm.command.system.*;
import com.tsconsulting.dsubbotin.tm.command.task.*;
import com.tsconsulting.dsubbotin.tm.command.user.*;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownArgumentException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownCommandException;
import com.tsconsulting.dsubbotin.tm.repository.*;
import com.tsconsulting.dsubbotin.tm.service.*;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService =
            new ProjectTaskService(projectRepository, taskRepository);

    private final ILogService logService = new LogService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthRepository authRepository = new AuthRepository();

    private final IAuthService authService = new AuthService(authRepository, userService);

    {
        registry(new DisplayCommand());
        registry(new AboutDisplayCommand());
        registry(new ArgumentsDisplayCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoDisplayCommand());
        registry(new VersionDisplayCommand());

        registry(new ProjectListShowCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectUpdateStatusByIdCommand());
        registry(new ProjectUpdateStatusByIndexCommand());
        registry(new ProjectUpdateStatusByNameCommand());

        registry(new TaskListShowCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskUpdateStatusByIdCommand());
        registry(new TaskUpdateStatusByIndexCommand());
        registry(new TaskUpdateStatusByNameCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskAllByProjectIdCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserIsAuthCommand());
        registry(new UserListShowCommand());
        registry(new UserLogInCommand());
        registry(new UserLogOutCommand());
        registry(new UserRegistryCommand());
        registry(new UserShowByIdCommand());
        registry(new UserShowByLoginCommand());
        registry(new UserUpdateByIdCommand());
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    public void run(final String[] args) {
        try {
            TerminalUtil.printMessage("** WELCOME TO TASK MANAGER **");
            initData();
            parseArgs(args);
            process();
        } catch (Exception e) {
            logService.error(e);
            System.exit(1);
        }
    }

    private void parseArgs(final String[] args) throws AbstractException {
        if (args == null || args.length == 0) return;
        AbstractCommand command = commandService.getCommandByName(args[0]);
        if (command == null) throw new UnknownArgumentException();
        command.execute();
        command = commandService.getCommandByName("exit");
        command.execute();
    }

    private void process() throws AbstractException {
        logService.debug("Test environment!");
        String command = "";
        while (!isExitCommand(command)) {
            command = TerminalUtil.nextLine();
            logService.command(command);
            runCommand(command);
            logService.info("Commands '" + command + "' executed!");
        }
    }

    private void runCommand(final String command) {
        try {
            if (EmptyUtil.isEmpty(command)) throw new UnknownCommandException();
            final AbstractCommand abstractCommand = commandService.getCommandByName(command);
            if (abstractCommand == null) throw new UnknownCommandException();
            abstractCommand.execute();
        } catch (Exception e) {
            logService.error(e);
        }
    }

    private void registry(final AbstractCommand command) {
        try {
            if (command == null) throw new UnknownCommandException();
            command.setServiceLocator(this);
            commandService.add(command);
        } catch (Exception e) {
            logService.error(e);
        }
    }

    private boolean isExitCommand(final String command) throws AbstractException {
        final AbstractCommand abstractCommand;
        abstractCommand = commandService.getCommandByName("exit");
        if (abstractCommand == null) return false;
        return abstractCommand.name().equals(command);
    }

    private void initData() {
        try {

            userService.create("admin", "admin", Role.ADMIN);
            userService.create("test", "test", Role.USER);

            String adminUserId = userService.findByLogin("admin").getId();
            String testUserId = userService.findByLogin("test").getId();

            projectService.create(adminUserId, "D_Project_1", "1");
            projectService.create(adminUserId, "C_Project_2", "2");
            projectService.create(adminUserId, "A_Project_3", "3");
            projectService.create(adminUserId, "B_Project_4", "4");
            projectService.create(adminUserId, "E_Project_5", "5");

            projectService.startByIndex(adminUserId, 1);
            projectService.finishByIndex(adminUserId, 2);

            taskService.create(adminUserId, "B_Task_1", "1");
            taskService.create(adminUserId, "A_Task_2", "2");
            taskService.create(adminUserId, "C_Task_3", "3");
            taskService.create(adminUserId, "E_Task_4", "4");
            taskService.create(adminUserId, "D_Task_5", "5");

            taskService.finishByIndex(adminUserId, 3);
            taskService.startByIndex(adminUserId, 4);

            projectTaskService.bindTaskToProject(
                    adminUserId,
                    projectRepository.findByIndex(adminUserId, 1).getId(),
                    taskRepository.findByIndex(adminUserId, 1).getId()
            );

            projectTaskService.bindTaskToProject(
                    adminUserId,
                    projectRepository.findByIndex(adminUserId, 1).getId(),
                    taskRepository.findByIndex(adminUserId, 2).getId()
            );

            projectTaskService.bindTaskToProject(
                    adminUserId,
                    projectRepository.findByIndex(adminUserId, 2).getId(),
                    taskRepository.findByIndex(adminUserId, 3).getId()
            );

            projectTaskService.bindTaskToProject(
                    adminUserId,
                    projectRepository.findByIndex(adminUserId, 3).getId(),
                    taskRepository.findByIndex(adminUserId, 4).getId()
            );
        } catch (AbstractException e) {
            logService.error(e);
        }
    }

}
