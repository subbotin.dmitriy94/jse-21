package com.tsconsulting.dsubbotin.tm.comparator;

import com.tsconsulting.dsubbotin.tm.api.entity.IHasStartDate;

import java.util.Comparator;

public final class ComparatorByStartDate implements Comparator<IHasStartDate> {

    public static final ComparatorByStartDate INSTANCE = new ComparatorByStartDate();

    private ComparatorByStartDate() {
    }

    @Override
    public int compare(IHasStartDate o1, IHasStartDate o2) {
        if (o1.getStartDate() == null) return 1;
        if (o2.getStartDate() == null) return -1;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }

}
